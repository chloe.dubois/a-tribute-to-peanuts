# TP8 - A tribute to ... Peanuts

Non, il ne s'agit pas de cacahuètes, mais bien de Snoopy et ses petits camarades :-)

## Contenu du dossier

Le dossier contient : 
- un fichier HTML,
- un dossier "assets" avec le scss, le css compilé et un dossier d'images,
- un dossier "js" avec le fichier javascript.

## Techno utilisées pour le projet

Les techno utilisées dans le projet sont les suivantes : 
- HTML,
- CSS / Sass, 
- javascript.

Pas de librairies ou de framework, bootstrap ou jquery


