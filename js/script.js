
// change nav color on scroll

const nav = document.getElementById('nav')

window.addEventListener('scroll', function() { 
    if (window.scrollY) { 
        nav.classList.add('nav-scroll');
    } else {
        nav.classList.remove('nav-scroll');
    }
});

// nav version small - collapse

const burger = document.getElementById('burger');
const navItems = document.getElementById('navItems');

burger.addEventListener('click', (myEvent) => {
    navItems.classList.toggle('collapse');
    burger.classList.toggle('collapse');
});



// fancy box : details sur les perso

const fancyBox = document.getElementById('custom-fancybox');
const characters = document.getElementsByClassName('charac');

for (let i = 0; i < characters.length; i += 1) {
    characters[i].addEventListener('click', (myEvent) => {
        fancyBox.classList.add('active');
        const targetCharac = myEvent.target.id;
        const targetDetails = document.getElementById(`${targetCharac}D`);
        targetDetails.classList.add('active');
    });
};

const details = document.getElementsByClassName('characDetails');
const close = document.getElementsByClassName('close');

for (let i = 0; i < close.length; i += 1) {
    close[i].addEventListener('click', (dismissFancy) => {  
        fancyBox.classList.remove('active');
        for (let i = 0; i < details.length; i += 1) {
            if (details[i].classList.contains("active")) {
                details[i].classList.remove('active');
            }
        };
    });
};
